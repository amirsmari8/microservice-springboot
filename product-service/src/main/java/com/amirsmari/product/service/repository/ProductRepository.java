package com.amirsmari.product.service.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.amirsmari.product.service.model.Product;

public interface ProductRepository extends MongoRepository<Product, String> {

}
